package hello;

import org.springframework.beans.factory.annotation.Value;

public class NetworkConfig {
	private String id;
	private String dataSourceName;
	
	@Value("${test.path}")
	private String path;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
