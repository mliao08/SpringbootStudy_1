package hello;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

public class GreetingServiceConfig {
	@Bean
	@ConditionalOnProperty(name="language.name",havingValue="English",matchIfMissing=true) /*根据配置属性判断*/
	public GreetingService englishGreetingService() {
		return new EnglishGreetingService();
	}
	
	@Bean
	@ConditionalOnProperty(name="language.name",havingValue="Chinese")
	public GreetingService chineseGreetingService() {
		return new ChineseGreetingService();
	}
}
