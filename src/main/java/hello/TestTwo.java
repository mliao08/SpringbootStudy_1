package hello;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/testtwo")
public class TestTwo {
	
	@RequestMapping(value="/getParam/{id}",method=RequestMethod.GET,produces="application/json")
	public String getParam(@PathVariable("id") String id) {
		System.out.println("url id = "+id);
		return id;
	}
	
	@RequestMapping(value="/getParamFromUrl",method=RequestMethod.GET,produces="application/json")
	public String getParamFromUrl(@RequestParam(value="id",defaultValue="123") int id){
		System.out.println("url id="+id);
		return String.valueOf(id);
	}
}
