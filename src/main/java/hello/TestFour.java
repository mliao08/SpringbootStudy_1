package hello;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/TestFour")
/*@ResponseBody 标记，将Controller的方法返回的对象，经由HttpMessageConverter转换成指定的格式后，写入到
 * response对象的body数据区*/
public class TestFour {
	@RequestMapping(value="/getParams",method=RequestMethod.POST,produces="application/json")
	Map<String,String> getParams(@RequestBody NetworkConfig config,HttpServletRequest request,HttpServletResponse response) {
		Map<String,String> map = new LinkedHashMap<String,String>();
		map.put("id", config.getId());
		map.put("dataSourceName", config.getDataSourceName());
		return map;
	}
}
