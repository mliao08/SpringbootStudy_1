package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/testthree")
public class TestThree {
	@Value("${application.name}")
	String appName;
	
	@Value("${application.id}")
	int appId;
	
	@RequestMapping(value="/getAppInfo",method=RequestMethod.GET)
	public String getAppInfo() {
		return new String("appName="+appName+", appId="+appId);
	}
}
