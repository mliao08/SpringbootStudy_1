package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(Application.class, args);
	}

}

/*
 * 1、类的映射                   D
 * 2、方法的映射	   D 
 * 3、从request url中获取变量值      D
 * 4、从request url中获取参数         D
 * 5、读取application.properties的配置到指定字段   D
 * 6、从request请求中获取body  @RequestBody 的使用，HttpMessageConverter将请求的body部分映射到被标记的对象上
 * */

