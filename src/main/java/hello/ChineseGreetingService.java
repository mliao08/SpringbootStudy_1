package hello;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Service
@Component
@Primary
public class ChineseGreetingService implements GreetingService{

	@Override
	public String greet() {
		// TODO Auto-generated method stub
		return "你好，世界！";
	}
	
	@PostConstruct  
	/*This method will be called after the java constructor*/
	public void init() {
		System.out.println("ChineseGreetingService:init");
	}
}
