package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value="/testone")
public class TestOne {
	
	@RequestMapping(value="/getReply",method=RequestMethod.POST)
	public String getReply() {
		System.out.println("testone:getReply");
		return "getReply";
	}
	
	@RequestMapping(value="getReplyParams",method=RequestMethod.POST,params="myparam=true",headers="Referer=http://www.ifeng.com/")
	public String getReplyParams() {
		System.out.println("getReplyParams");
		return "getReplyParams";
	}
}


/*RequestMapping 的参数说明：
 * value：请求路径
 * method：RequestMethod 中指定的请求方法
 * consumes：指定处理的Content-Type
 * produces：指定返回的body的内容类型，需要客户端的Accept中支持
 * params：指定request url中必须包含的参数值。才能处理该请求
 * headers：指定request url中必须包含的header值，才能处理该请求
 * */