package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/")
public class TestAutowireByInterface {
	@Autowired
	GreetingService greetingService;
	
	@RequestMapping(value="/home",method=RequestMethod.POST)
	public String home() {
		return greetingService.greet();
	}
}
