package hello;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/test-five")
public class TestFive {
	@RequestMapping(value="/get-config",method=RequestMethod.POST)
	public String testConfigGetting(@RequestBody NetworkConfig config) {
		
		return "success";
	}
}
